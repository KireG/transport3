
      $(document).ready(function(){

        //Load trips------------------
      function loadTable(){  
        $.ajax({
            method: "GET",
            url   : "/api/show",
            dataType: "json"
        }).done(function(d){
            //console.log(d[1].reference);
            for(i=0; i<d.length; i++)
            {
            let content= `<tr>
            <td style="padding-top: 20px;">${d[i].id}</td>
            <td><input class="cl_${d[i].id} tripscl" type="text" id ="date_${d[i].id}" value="${d[i].date}" readonly></td>
            <td><input class="cl_${d[i].id} tripscl" type="text" id ="reference_${d[i].id}" value="${d[i].reference}" readonly></td>
            <td><input class="cl_${d[i].id} tripscl" type="text" id ="customer_${d[i].id}" value="${d[i].customer}" readonly></td>
            <td><input class="cl_${d[i].id} tripscl" type="text" id ="begin_country_${d[i].id}" value="${d[i].begin_country}" readonly></td>
            <td><input class="cl_${d[i].id} tripscl" type="text" id ="end_country_${d[i].id}" value="${d[i].end_country}" readonly></td>
            <td><input class="cl_${d[i].id} tripscl" type="text" id ="route_${d[i].id}" value="${d[i].route}" readonly></td>
            <td><input class="cl_${d[i].id} tripscl" type="text" id ="registration_${d[i].id}" value="${d[i].registration}" readonly></td>
            <td><input class="cl_${d[i].id} tripscl" type="text" id ="additional_info_${d[i].id}" value="${d[i].additional_info}" readonly></td>
            <td><input class="cl_${d[i].id} tripscl" type="text" id ="driver_${d[i].id}" value="${d[i].driver}" readonly></td>
            <td><input class="cl_${d[i].id} tripscl tripscl2" type="text" id ="number_of_shipments_${d[i].id}" value="${d[i].number_of_shipments}" readonly></td>
            <td><input class="cl_${d[i].id} tripscl tripscl2" type="text" id ="kilometers_${d[i].id}" value="${d[i].kilometers}" readonly></td>
            <td><input class="cl_${d[i].id} tripscl tripscl2" type="text" id ="tons_${d[i].id}" value="${d[i].tons}" readonly></td>
            <td><div class="input-group-edit" id="ige_${d[i].id}"><Button class="btnEdit"  data-id='${d[i].id}' id='btnEdit_${d[i].id}' data-id='${d[i].id}'>Edit</Button></div></td>
            <td class="tdc"><Button class="deletebtn" id='delbtn${d[i].id}' data-id='${d[i].id}' data-bs-toggle="modal" data-bs-target="#exampleModal">Delete</Button></td>
            </tr>`;
            $('#tab').append(content);
            }
         });
      }
      loadTable();

    //open delete modal with id for delete-----
          $('#tab').on('click', '.deletebtn', function (){
            var passedID=$(this).data('id');
            $('.deltripbtn').attr("data-id", passedID);
      });  

    //Delete trip
     $('.deltripbtn').on('click', function(){
      var passedID=$(this).data('id');
      
      $.ajax({
              url:"api/delete"+"/"+ passedID,
              method:"DELETE",
              data: {
                "_token": "{{ csrf_token() }}",
              },
              success: function(data){
              $('#tab').empty();
              loadTable();
              $("#succesdivdelete").css("display","block");
              $("#succesdivdelete").fadeOut(5000);
           }
        })
     });

      //add row function
      $('#addnewtrip').on('submit', function(event){
        event.preventDefault();
        date = $('#date').val();
        reference = $('#reference').val();
        customer = $('#customer').val();
        begin_country = $('#begin_country').val();
        end_country = $('#end_country').val();
        route = $('#route').val();
        vehicle = $('#vehicle').val();
        additional_info = $('#additional_info').val();
        driver = $('#driver').val();
        number_of_shipments = $('#number_of_shipments').val();
        kilometers = $('#kilometers').val();
        tons = $('#tons').val();
        if(date == '' || reference == '' || customer == '' || begin_country == '' || end_country == '' || route == '' || vehicle == '' || additional_info == '' || driver == '' || number_of_shipments == '' || kilometers == ''  || tons == ''){
          $("#errordiv").css("display","block");
          $("#addModal").animate({scrollTop: 0}, "slow");
          $("#errordiv").fadeOut(3000);
        }else{
        $.ajax({
            url:"{{ route('insert') }}",
            method:"POST",
            data:{
            "_token": "{{ csrf_token() }}",
            date: date,
            reference: reference,
            customer_id: customer,
            begin_country: begin_country,
            end_country: end_country,
            route: route,
            vehicle_id: vehicle,
            additional_info: additional_info,
            driver_id: driver,
            number_of_shipments: number_of_shipments,
            kilometers: kilometers,
            tons: tons
          },
          success: function(data){
              $("#addnewtrip")[0].reset();
              $("#addModal").modal('hide');
              $('#tab').empty();
              loadTable();
              $("#succesdiv").css("display","block");
              $("#succesdiv").fadeOut(5000);
          }
        })
      }   
    });

    //edit function______________
      $('#tab').on('click', '.btnEdit', function (e) {
        e.preventDefault();
      
        var justId =  $(this).data('id');
        //var atrId =  $(this).data('id');
        //var justId = $(this).data('id').slice(4).trim();
        //alert(justId);
        var dateTx = 'date_' + justId;
        var referenceTx = 'reference_' + justId;
        var customerTx = 'customer_' + justId;
        var begin_countryTx = 'begin_country_' + justId;
        var end_countryTx = 'end_country_' + justId;
        var routeTx = 'route_' + justId;
        var registrationTx = 'registration_' + justId;
        var additional_infoTx = 'additional_info_' + justId;
        var driverTx = 'driver_' + justId;
        var number_of_shipmentsTx = 'number_of_shipments_' + justId;
        var kilometersTx = 'kilometers_' + justId;
        var tonsTx = 'tons_' + justId;
        var currRow = 'row_' + justId;
        
        
        
        inBtn = $(this)[0].id;
        //inBtn = $(this).id;
        if (inBtn == 'btnEdit_' + justId){
            var buttonEdit = 'btnEdit_' + justId;
            $('')
            $('#'+buttonEdit).prop('innerHTML', 'Save');
            //$('#'+buttonEdit).css('background-color', 'white');
            $('#'+buttonEdit).css('color', 'red');
            $('#'+buttonEdit).attr("id","btnSave_"+justId);
            $('#'+dateTx).prop('readonly', false);
            $('#'+referenceTx).prop('readonly', false);
            $('#'+customerTx).prop('readonly', false);
            $('#'+begin_countryTx).prop('readonly', false);
            $('#'+end_countryTx).prop('readonly', false);
            $('#'+routeTx).prop('readonly', false);
            $('#'+registrationTx).prop('readonly', false);
            $('#'+additional_infoTx).prop('readonly', false);
            $('#'+driverTx).prop('readonly', false);
            $('#'+number_of_shipmentsTx).prop('readonly', false);
            $('#'+kilometersTx).prop('readonly', false);
            $('#'+tonsTx).prop('readonly', false);
            $('.cl_'+justId).css('background-color', 'white');
        }
        else{
            var buttonSave = 'btnSave_' + justId;
            $('#'+buttonSave).prop('innerHTML', 'Edit');
            //$('#'+buttonSave).css('background-color', 'rgba(0, 0, 0, 0.075)');
            $('#'+buttonSave).css('color', 'black');
            $('#'+buttonSave).attr("id","btnEdit_"+justId);
            $('#'+dateTx).prop('readonly', true);
            $('#'+referenceTx).prop('readonly', true);
            $('#'+customerTx).prop('readonly', true);
            $('#'+begin_countryTx).prop('readonly', true);
            $('#'+end_countryTx).prop('readonly', true);
            $('#'+routeTx).prop('readonly', true);
            $('#'+registrationTx).prop('readonly', true);
            $('#'+additional_infoTx).prop('readonly', true);
            $('#'+driverTx).prop('readonly', true);
            $('#'+number_of_shipmentsTx).prop('readonly', true);
            $('#'+kilometersTx).prop('readonly', true);
            $('#'+tonsTx).prop('readonly', true);
            $('.cl_'+justId).css('background-color', 'rgb(197, 233, 223)');

            date = $('#'+dateTx).val();
            reference = $('#'+referenceTx).val();
            customer = $('#'+customerTx).val();
            begin_country = $('#'+begin_countryTx).val();
            end_country = $('#'+end_countryTx).val();
            route = $('#'+routeTx).val();
            vehicle = $('#'+registrationTx).val();
            additional_info = $('#'+additional_infoTx).val();
            driver = $('#'+driverTx).val();
            number_of_shipments = $('#'+number_of_shipmentsTx).val();
            kilometers = $('#'+kilometersTx).val();
            tons = $('#'+tonsTx).val();
      
        //       varBlood_type = $('#'+btTx)[0].value;
            $.ajax({

                url: "api/edit"+"/"+justId,
                method: "POST",
                dataType: "json",
                data:{
                "_token": "{{ csrf_token() }}",
                date: date,
                reference: reference,
                customer_id: customer,
                begin_country: begin_country,
                end_country: end_country,
                route: route,
                vehicle_id: vehicle,
                additional_info: additional_info,
                driver_id: driver,
                number_of_shipments: number_of_shipments,
                kilometers: kilometers,
                tons: tons
              },
                 success: function(data) {         
        //         },
        //         error: function() {
        //             alert('There was some error performing the AJAX call!');
                 }
               });
               console.log(date,reference,
                customer,
                begin_country,
                end_country,
                route,
                vehicle,
                additional_info,
                driver,
                number_of_shipments,kilometers,tons);
         }
        });

      //Search 
            $('.search-input').on('keyup', function(){
                  var searchQuery = $(this).val();
                  if (searchQuery !== ''){
                      $('#tab').hide();
                  }else if(searchQuery == ''){
                      $('#tab').show();
                  }
                   $.ajax({
                      type: "POST",
                      url: "{{ route('live_search') }}",
                      data: {
                          '_token': '{{ csrf_token() }}',
                          searchQuery: searchQuery,
                      },
                      dataType: "json",
                      success: function(res){
                          var tripList = '';
                          $('#searchList').html('');
                          $.each(res, function(index, d){
                            console.log(d);
                              tripList =  
                              `<tr><td style="padding-top: 20px;">${d[i].id}</td><td><input class="cl_${d[i].id} tripscl" type="text" id ="date_${d[i].id}" value="${d[i].date}" readonly></td><td><input class="cl_${d[i].id} tripscl" type="text" id ="reference_${d[i].id}" value="${d[i].reference}" readonly></td><td><input class="cl_${d[i].id} tripscl" type="text" id ="customer_${d[i].id}" value="${d[i].customer}" readonly></td><td><input class="cl_${d[i].id} tripscl" type="text" id ="begin_country_${d[i].id}" value="${d[i].begin_country}" readonly></td><td><input class="cl_${d[i].id} tripscl" type="text" id ="end_country_${d[i].id}" value="${d[i].end_country}" readonly></td><td><input class="cl_${d[i].id} tripscl" type="text" id ="route_${d[i].id}" value="${d[i].route}" readonly></td><td><input class="cl_${d[i].id} tripscl" type="text" id ="registration_${d[i].id}" value="${d[i].registration}" readonly></td><td><input class="cl_${d[i].id} tripscl" type="text" id ="additional_info_${d[i].id}" value="${d[i].additional_info}" readonly></td><td><input class="cl_${d[i].id} tripscl" type="text" id ="driver_${d[i].id}" value="${d[i].driver}" readonly></td><td><input class="cl_${d[i].id} tripscl tripscl2" type="text" id ="number_of_shipments_${d[i].id}" value="${d[i].number_of_shipments}" readonly></td><td><input class="cl_${d[i].id} tripscl tripscl2" type="text" id ="kilometers_${d[i].id}" value="${d[i].kilometers}" readonly></td><td><input class="cl_${d[i].id} tripscl tripscl2" type="text" id ="tons_${d[i].id}" value="${d[i].tons}" readonly></td><td><div class="input-group-edit" id="ige_${d[i].id}"><Button class="btnEdit"  data-id='${d[i].id}' id='btnEdit_${d[i].id}' data-id='${d[i].id}'>Edit</Button></div></td><td class="tdc"><Button class="deletebtn" id='delbtn${d[i].id}' data-id='${d[i].id}' data-bs-toggle="modal" data-bs-target="#exampleModal">Delete</Button></td></tr>`;
                              $('#searchList').append(tripList);
                          });
                      }
                 });    
              });    
   


      });
    