<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
           ['name'=>'LKW Walter','address'=>'IZ NO-Sud, Straße 14,Vienna','country_id'=> 3],
           ['name'=>'LKW Walter','address'=>'Vojensvej 4b, 6500 Vojens','country_id'=> 4],
        ]);
    }
}
