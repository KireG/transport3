<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicles')->insert([
          ['registration'=>'KH0039BX','manufacturer'=>'Man','model'=>'TGX','chasy_number'=>'WMA22334775983','year'=>2017,'euro_norm_id'=>3],
          ['registration'=>'KH7685BT','manufacturer'=>'Man','model'=>'TGX','chasy_number'=>'WMA22334745567','year'=>2016,'euro_norm_id'=>3],
        ]);
    }
}
