<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoices')->insert([
            'number'=>1034,'customer_id'=>1,'amount'=>1500,'created'=>'2021-12-01','due_date'=>'2021-12-15'
        ]);
    }
}
