<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Seeder;

class CostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('costs')->insert([
            ['trip_id'=>1,'drivers_cost'=>500,'fuel'=>800,'toll'=>150,'additional'=>80],
            ['trip_id'=>2,'drivers_cost'=>700,'fuel'=>500,'toll'=>110,'additional'=>90],
            ['trip_id'=>3,'drivers_cost'=>600,'fuel'=>900,'toll'=>170,'additional'=>50],
            ['trip_id'=>4,'drivers_cost'=>800,'fuel'=>400,'toll'=>160,'additional'=>85]
        ]);
    }
}
