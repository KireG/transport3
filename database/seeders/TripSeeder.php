<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TripSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trips')->insert([
         ['date'=>'2021-11-03','reference'=>'1041-011-0043','begin_country'=>2,'end_country'=>3,'route'=>'dsjhfkjshgdkjshk','driver_id'=>1,'customer_id'=>1,'vehicle_id'=>1,'additional_info'=>'HROA9887','number_of_shipments'=>2,'kilometers'=>1345,'tons'=>24],
         ['date'=>'2021-11-10','reference'=>'1043-011-0067','begin_country'=>3,'end_country'=>2,'route'=>'dsjhfkjshgdkjshk','driver_id'=>2,'customer_id'=>2,'vehicle_id'=>2,'additional_info'=>'HROA9885','number_of_shipments'=>1,'kilometers'=>950,'tons'=>21],
         ['date'=>'2021-11-15','reference'=>'1042-011-0055','begin_country'=>2,'end_country'=>4,'route'=>'dsjhfkjshgdkjshk','driver_id'=>1,'customer_id'=>1,'vehicle_id'=>1,'additional_info'=>'HROA9886','number_of_shipments'=>1,'kilometers'=>1560,'tons'=>20],
         ['date'=>'2021-11-23','reference'=>'1040-011-0040','begin_country'=>2,'end_country'=>1,'route'=>'dsjhfkjshgdkjshk','driver_id'=>2,'customer_id'=>2,'vehicle_id'=>2,'additional_info'=>'HROA9877','number_of_shipments'=>1,'kilometers'=>2000,'tons'=>12],
         ['date'=>'2021-11-04','reference'=>'1041-011-0049','begin_country'=>1,'end_country'=>2,'route'=>'dsjhfkjshgdkjshk','driver_id'=>1,'customer_id'=>1,'vehicle_id'=>1,'additional_info'=>'HROA9887','number_of_shipments'=>2,'kilometers'=>1345,'tons'=>24],
         ['date'=>'2021-12-17','reference'=>'1043-012-0087','begin_country'=>4,'end_country'=>4,'route'=>'dsjhfkjshgdkjshk','driver_id'=>2,'customer_id'=>2,'vehicle_id'=>2,'additional_info'=>'HROA9885','number_of_shipments'=>1,'kilometers'=>950,'tons'=>21],
         ['date'=>'2021-12-19','reference'=>'1042-012-0155','begin_country'=>3,'end_country'=>1,'route'=>'dsjhfkjshgdkjshk','driver_id'=>1,'customer_id'=>1,'vehicle_id'=>1,'additional_info'=>'HROA9886','number_of_shipments'=>1,'kilometers'=>1560,'tons'=>20],
         ['date'=>'2021-12-21','reference'=>'1040-012-0240','begin_country'=>3,'end_country'=>2,'route'=>'dsjhfkjshgdkjshk','driver_id'=>2,'customer_id'=>2,'vehicle_id'=>2,'additional_info'=>'HROA9877','number_of_shipments'=>1,'kilometers'=>2000,'tons'=>12],
         ['date'=>'2021-12-13','reference'=>'1041-012-0049','begin_country'=>1,'end_country'=>3,'route'=>'dsjhfkjshgdkjshk','driver_id'=>1,'customer_id'=>1,'vehicle_id'=>1,'additional_info'=>'HROA9887','number_of_shipments'=>2,'kilometers'=>1345,'tons'=>24],
         ['date'=>'2021-12-19','reference'=>'1043-012-0167','begin_country'=>2,'end_country'=>5,'route'=>'dsjhfkjshgdkjshk','driver_id'=>2,'customer_id'=>2,'vehicle_id'=>2,'additional_info'=>'HROA9885','number_of_shipments'=>1,'kilometers'=>950,'tons'=>21],
         ['date'=>'2021-12-25','reference'=>'1042-012-0085','begin_country'=>3,'end_country'=>4,'route'=>'dsjhfkjshgdkjshk','driver_id'=>1,'customer_id'=>1,'vehicle_id'=>1,'additional_info'=>'HROA9886','number_of_shipments'=>1,'kilometers'=>1560,'tons'=>20],
         ['date'=>'2021-12-13','reference'=>'1040-012-0340','begin_country'=>1,'end_country'=>2,'route'=>'dsjhfkjshgdkjshk','driver_id'=>2,'customer_id'=>2,'vehicle_id'=>2,'additional_info'=>'HROA9877','number_of_shipments'=>1,'kilometers'=>2000,'tons'=>12],
        ]);
    }
}
